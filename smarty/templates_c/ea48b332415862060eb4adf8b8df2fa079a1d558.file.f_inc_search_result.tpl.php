<?php /* Smarty version Smarty-3.1.17, created on 2021-09-28 18:43:38
         compiled from "./inc/views/f_inc_search_result.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1430386070614e21cf44ebe2-56465468%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ea48b332415862060eb4adf8b8df2fa079a1d558' => 
    array (
      0 => './inc/views/f_inc_search_result.tpl',
      1 => 1632847402,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1430386070614e21cf44ebe2-56465468',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_614e21cfaca239_59583090',
  'variables' => 
  array (
    'lang' => 0,
    'filter' => 0,
    'translation' => 0,
    'B_startXXX' => 0,
    'data' => 0,
    'val' => 0,
    'B_del' => 0,
    'B_detailCategoryPage' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_614e21cfaca239_59583090')) {function content_614e21cfaca239_59583090($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_truncate')) include '/var/www/clients/client30/web1949/web/core/libs/Smarty/plugins/modifier.truncate.php';
?>




<div class="container d-none d-lg-block mb-4 mt-0"
	 style="background: #222850;"> 
	<div class="row justify-content-center align-items-center py-4 border-1">
		<div class="row" style="padding-top: -260px;">
			<div class="col-md-12 center-block">
				<form method="post" action="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/kategorie/suche/" style="text-align: center;">
                    <?php if ($_smarty_tpl->tpl_vars['filter']->value['postalcode']) {?>
						<input id="xsearch" type="text" class="form-control" name="filter[postalcode]"
							   value="<?php echo $_smarty_tpl->tpl_vars['filter']->value['postalcode'];?>
"
							   style="width:720px; height:56px; display:inline; border:0; border-radius:0;">
						<button type="submit"
								style="position:absolute;margin-left:-65px; background:#fff; color:#888; height:55px; border:0; border-radius:0;"
								class="btn btn-default blue_btn"><i class="fas fa-search fa-2x"></i></button>
                    <?php } else { ?>
						<input id="xsearch" type="text" class="form-control" name="filter[postalcode]"
							   placeholder="<?php echo $_smarty_tpl->tpl_vars['translation']->value['search_placeholder'];?>
"
							   style="width:720px; height:56px; display:inline; border:0; border-radius:0;">
						<button type="submit"
								style="position:absolute;margin-left:-65px; background:#fff; color:#888; height:55px; border:0; border-radius:0;"
								class="btn btn-default blue_btn"><i class="fas fa-search fa-2x"></i></button>
                    <?php }?>
				</form>
			</div>
		</div>
	</div>
</div>

            
            
            
<div class="container">
       
        <br>
        <br>


	<div class="row">
		<div class="col">
            <?php if ($_smarty_tpl->tpl_vars['B_startXXX']->value) {?>
				<h1><?php echo $_smarty_tpl->tpl_vars['translation']->value['new_vacations'];?>
</h1>
				<br>
            <?php }?>

            <?php if (count($_smarty_tpl->tpl_vars['data']->value)>0) {?>
				<div class="card-deck">
                    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['val']->key;
?>
						<div class="card mb-5">
							<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"><img class="card-img-top" src="<?php echo $_smarty_tpl->tpl_vars['val']->value['file1_700'];?>
"
																	   alt="<?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['val']->value['title'],50,"...",true);?>
"></a>
							<div class="card-body">
                                <?php if (($_smarty_tpl->tpl_vars['lang']->value=='en')&&(preg_match_all('/[^\s]/u',$_smarty_tpl->tpl_vars['val']->value['titleen'], $tmp)!=0)) {?>
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/">
                                    <h3 class="card-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['val']->value['titleen'],50,"...",true);?>
</h3></a>
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"><p class="card-text"
																			 style="color:#000;"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['val']->value['contenten'],200,"...",true);?>
</p>
									</a>
                                <?php } else { ?>
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"><h3
												class="card-title"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['val']->value['title'],50,"...",true);?>
</h3></a>
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"><p class="card-text"
																			 style="color:#000;"><?php echo smarty_modifier_truncate($_smarty_tpl->tpl_vars['val']->value['content'],200,"...",true);?>
</p>
									</a>
                                <?php }?>

							</div>

							<div class="card-footer">
								<p><span class="big-text">&nbsp;<?php echo $_smarty_tpl->tpl_vars['val']->value['days'];?>
</span>
                                    <span class="small-text" style="text-transform: uppercase;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['translation']->value['nights'];?>
</span>&nbsp;&nbsp;&nbsp;
                                    <span class="big-text"><?php echo $_smarty_tpl->tpl_vars['val']->value['persons'];?>
</span>
                                    <span class="small-text" style="text-transform: uppercase;">&nbsp;<?php echo $_smarty_tpl->tpl_vars['translation']->value['persons'];?>
</span>

                                
                                <?php if ($_smarty_tpl->tpl_vars['val']->value['type']=="offer") {?>
									<p style="margin-top: 25px;"><a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/">
											<button type="button" name="bidd" class="btn btn-success width-100"><i
														class="fas fa-check-circle fa-2x"
														style="margin-right:10px; vertical-align:middle;"></i><?php echo $_smarty_tpl->tpl_vars['translation']->value['button_submit_price_suggestion'];?>

											</button>
										</a></p>
									<br>
                                <?php } else { ?>
									<p style="margin-top: 25px;"><a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/artikel/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/">
											<button type="button" name="bidd" class="btn btn-info width-100"><i
														class="fas fa-thumbs-up fa-2x"
														style="margin-right:10px; vertical-align:middle;"></i><?php echo $_smarty_tpl->tpl_vars['translation']->value['button_submit_draw'];?>

											</button>
										</a></p>
									<br>
                                <?php }?>
							</div>
                            <?php if ($_smarty_tpl->tpl_vars['val']->value['type']=="offer"&&$_smarty_tpl->tpl_vars['B_del']->value) {?>
								<div class="card-footer">
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/delete-ad/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"
									   onclick="return confirm('<?php echo $_smarty_tpl->tpl_vars['translation']->value['confirm_del'];?>
');">
										<button type="button" class="btn btn-danger"><?php echo $_smarty_tpl->tpl_vars['translation']->value['delete_ad'];?>
</button>
									</a>
								</div>
                            <?php } elseif ($_smarty_tpl->tpl_vars['B_del']->value) {?>
								<div class="card-footer">
									<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/delete-ad/<?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
/"
									   onclick="return confirm('<?php echo $_smarty_tpl->tpl_vars['translation']->value['confirm_del'];?>
');">
										<button type="button" class="btn btn-secondary"
												disabled><?php echo $_smarty_tpl->tpl_vars['translation']->value['delete_ad'];?>
</button>
									</a>
								</div>
                            <?php }?>
						</div>
                    <?php } ?>

                    <?php $_smarty_tpl->tpl_vars['missingCards'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['missingCards']->step = 1;$_smarty_tpl->tpl_vars['missingCards']->total = (int) ceil(($_smarty_tpl->tpl_vars['missingCards']->step > 0 ? 3+1 - ((count($_smarty_tpl->tpl_vars['data']->value)+1)) : (count($_smarty_tpl->tpl_vars['data']->value)+1)-(3)+1)/abs($_smarty_tpl->tpl_vars['missingCards']->step));
if ($_smarty_tpl->tpl_vars['missingCards']->total > 0) {
for ($_smarty_tpl->tpl_vars['missingCards']->value = (count($_smarty_tpl->tpl_vars['data']->value)+1), $_smarty_tpl->tpl_vars['missingCards']->iteration = 1;$_smarty_tpl->tpl_vars['missingCards']->iteration <= $_smarty_tpl->tpl_vars['missingCards']->total;$_smarty_tpl->tpl_vars['missingCards']->value += $_smarty_tpl->tpl_vars['missingCards']->step, $_smarty_tpl->tpl_vars['missingCards']->iteration++) {
$_smarty_tpl->tpl_vars['missingCards']->first = $_smarty_tpl->tpl_vars['missingCards']->iteration == 1;$_smarty_tpl->tpl_vars['missingCards']->last = $_smarty_tpl->tpl_vars['missingCards']->iteration == $_smarty_tpl->tpl_vars['missingCards']->total;?>
						<div class="card">
						</div>
                    <?php }} ?>
				</div>
            <?php } elseif ($_smarty_tpl->tpl_vars['B_detailCategoryPage']->value) {?>
				<h1><?php echo $_smarty_tpl->tpl_vars['translation']->value['no_offers_yet'];?>
</h1>
				<a href="/<?php echo $_smarty_tpl->tpl_vars['lang']->value;?>
/inserat-erstellen/">
					<button type="button" class="btn btn-success mt-5"><?php echo $_smarty_tpl->tpl_vars['translation']->value['create_first_offer'];?>
</button>
				</a>
            <?php }?>
		</div>
	</div>
</div>

<?php echo $_smarty_tpl->getSubTemplate ("f_page_links.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }} ?>
