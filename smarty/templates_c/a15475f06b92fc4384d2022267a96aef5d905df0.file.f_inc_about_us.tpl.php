<?php /* Smarty version Smarty-3.1.17, created on 2021-09-27 17:59:59
         compiled from "./inc/views/f_inc_about_us.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16374959306017fedfcd71f7-63247526%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a15475f06b92fc4384d2022267a96aef5d905df0' => 
    array (
      0 => './inc/views/f_inc_about_us.tpl',
      1 => 1632480405,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16374959306017fedfcd71f7-63247526',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_6017fedfd66d60_50827783',
  'variables' => 
  array (
    'lang' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_6017fedfd66d60_50827783')) {function content_6017fedfd66d60_50827783($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['lang']->value=='de') {?>



    
	<br>
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>WER WIR SIND</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7 pr-lg-5 mb-5">
                
                <br>
                Vielleicht das Wichtigste vorab: 
                <br><br>
                <strong>Nein, wir sind keine Urlaubsvermittler, keine Reiseanbieter, keine Preisvergleichsseite und auch kein Reiseschnäppchenportal</strong>.
                <br><br>
                Was wir jedoch schon sind: Ein kleines Team von Marketing-, Vertriebs- und IT- Enthusiasten mit Sitz am
                herrlichen Wörthersee in Kärnten, dass sich damit beschäftigt, Ihnen als gewerblichen Anbieter eine <strong>Marketingplattform</strong> zur Verfügung zu stellen, welche Ihnen die direkte Kommunikation mit Interessenten strukturiert und nachvollziehbar ermöglicht.
                <br><br>
                Eine Marketingplattform bei welcher Sie als Anbieter im Vordergrund stehen und das Erhöhen der Sichtbarkeit Ihrer Webseite und damit zusammenhängend das Direktbuchen oberste Priorität haben.<br><br>

				

				Wir bei hobidd sehen uns als verlängerter Marketingarm von Anbietern und setzen unsere Fachwissen ein, um Ihre Angebote in Szene zu setzen, über verschiedene digitale Kanäle auszuspielen und vorerst in Österreich, Deutschland und der Schweiz sichtbar zu machen.
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col">
						<h2>HOBIDD IST EINE "GAST TRIFFT HOTEL" PLATTFORM</h2>
					</div>
				</div>

				Die besten Urlaube sind bekanntlich dort, wo wir als Gast im Mittelpunkt stehen und die persönliche Note
				des Hauses nicht zu kurz kommt.
				<br>
				<br>

				Doch wie findet man solch ausgewählte Reiseziele?
				<br>
				<br>

				Genau dieser Frage sind wir bei hobidd nachgegangen. Unsere Mission: Wir bringen Reisende und Anbieter
				zusammen – schnell, reibungslos und fair. Das Urlaubsfeeling soll schon vor dem Urlaub aufkommen und
				eine Win-Win Situation für alle Beteiligten sein!
				<br>
				<br>
				<br>
				<div class="row">
					<div class="col">
						<h2>WAS BEDEUTET HOBIDD UND WARUM SIND WIR ANDERS</h2>
					</div>
				</div>

				Der Name hobidd ist eine Abkürzung für „HOLIDAY BIDDINGS“, denn was unsere Plattform ausmacht ist, dass
				potenzielle Gäste deren Wunschpreis mit dem jeweiligen Anbieter direkt aushandeln können – und zwar mit
				bis zu 5 Preisvorschlägen pro Urlaubsangebot. Somit wird sichergestellt, dass beide Seiten den für sie
				bestmöglichen Preis erzielen.
				<br>
				<br>
				Anders als Buchungsplattformen, stellen wir den direkten Kontakt zwischen Reisenden und Anbietern her.
				Weder spielen wir den Mittelsmann noch verrechnen wir Gebühren für getroffene Vereinbarungen oder
				mischen uns in der Preisgestaltung ein. Stattdessen versichern wir eine erhöhte Angebotsreichweite,
				steigern die Sichtbarkeit der Anbieterwebseite und ermöglichen mehr Direktbuchungen.
				<br>
				<br>
				<br>

				<div class="row">
					<div class="col">
						<h2>UNSERE WERTE - UNSER SERVICE - DAS HERZ VON HOBIDD</h2>
					</div>
				</div>

				Unsere Leidenschaft ist das Online Marketing und wir setzen alles daran den digitalen Werbeauftritt für
				Anbieter zu erleichtern. In weniger als 5 Minuten können Angebote, die professionell und ansprechend
				aussehen, online gestellt und in weiterer Folge auf Facebook und/oder Twitter geteilt oder in eigene
				Email-Newsletter eingebunden werden.
				<br>
				<br>

				Wir wollen das Direktbuchen so einfach wie möglich machen. Und das ohne Vermittlungsprovisionen, zu
				leistbaren Fixpreisen. Reisende registrieren sich kostenlos, Anbieter wählen das für sie geeignete
				Paket.
				<br>
				<br>
				</span>
			</div>
			<div class="col-lg-5 mb-5">
				<img class="img-fluid mx-auto d-block" src="/img/ABOUT/office-grey.png">
			</div>
		</div>
	</div>
<?php } else { ?>
    
    <br>
	<div class="container">
		<div class="row">
			<div class="col">
				<h2>HOBIDD - WHO WE ARE</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7 pr-lg-5 mb-5">
                <br>
				Perhaps the most important in advance: <strong>
                <br>
                <br>
                No, we are neither a agency, a travel operator, a price comparison nor a bargain travel site. </strong> 
                
                <br>
                <br>
                What we are : A small team of marketing, sales and IT enthusiasts based on the beautiful Wörthersee in Carinthia/Austria, which works on it to provide you as a commercial provider with an <strong>interactive marketing platform</strong> which allows you to communicate directly with any interested parties of your offered packages in a structured and transparent way.
                <br><br>
                A marketing platform where you as a provider are paramounted and the increasing of your websites visibility and therefore the rising of direct booking have top priority.
                <br>
                <br>
                We at hobidd see ourselves as an extended marketing arm of the respective provider using our digital marketing expertise to stage their offers, play them out via various digital channels and to make them visible in Austria, Germany and Switzerland.
                <br><br>
                
				

				 
                
                
				<br>
				
				<div class="row">
					<div class="col">
						<h2>WHAT DOES HOBIDD STAND FOR, AND WHY ARE WE DIFFERENT?</h2>
					</div>
				</div>

				hobidd is an abbreviation of “HOLIDAY BIDDINGS”, and what sets our platform apart from others, is that
				interested parties is given the option to submit their desired rates for offered stays directly to you as provider. This ensures that both sides can agree on the best mutually
				acceptable price.
				<br>
				<br>
				Unlike booking and comparision platforms, we connect guests and providers directly with each other. We neither play the
				middle-man, nor do we charge commission on agreed contracts, or involve ourselves in price-setting.
				Instead, we ensure greater offer-reach, increased visibility of the client’s website, and make enable
				more direct bookings!
				<br>
				<br>
				<br>

				<div class="row">
					<div class="col">
						<h2>OUR VALUES – OUR SERVICE – HOBIDD’S CORE FOCUS</h2>
					</div>
				</div>

				Online Marketing is our passion, and we do everything possible to make it easier for our vendors to
				build an online advertising presence. It takes less than five minutes to publish a professional and
				attractive offer online, which can then be shared on Facebook and/or Twitter or included in the client’s
				newsletter.
				<br>
				<br>
                Furthermore we have created a very clear, standardized structure to handle all kind of communication procedures including responses on received requests up to booking confirmations directly between providers and interested parties.
                <br>
                <br>

				We want to facilitate direct bookings as much as possible – without taking commission, and at an
				affordable fixed price. Travellers can register for free. Vendors can choose the package most suitable
				to their needs.
				<br>
				<br>
				</span>
			</div>
			<div class="col-lg-5 mb-5">
				<img class="img-fluid mx-auto d-block" src="/img/ABOUT/office-grey.png">
			</div>
		</div>
	</div>
<?php }?>
<?php }} ?>
