<?php /* Smarty version Smarty-3.1.17, created on 2021-09-25 12:06:28
         compiled from "./inc/views/f_inc_for_travellers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1624819212614ef4a456f119-10888182%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4516f072c30a93053d885f719df3bdea6dc62204' => 
    array (
      0 => './inc/views/f_inc_for_travellers.tpl',
      1 => 1632480405,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1624819212614ef4a456f119-10888182',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_614ef4a473a624_39696398',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_614ef4a473a624_39696398')) {function content_614ef4a473a624_39696398($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['lang']->value=='de') {?>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-lg-7 pr-lg-8 mb-5 mt-0">

				<h2>ENTDECKE NEUE REISEZIELE</h2>Suche nach Hotels, Orten oder Ländern oder nutze einfach die
				Volltextsuche um gezielt nach Lastminuteangeboten oder Themenbereichen zu suchen. Einfacher geht es
				nicht.
				<br><br><br>

                

				<p class="mt-3">
				<h2>HANDLE DEINEN PREIS AUS</h2>Ist das passende Package gefunden, hast du nach kostenloser
				Registrierung die Möglichkeit jeweils bis zu 5 Preisvorschläge dafür abzugeben. Du besprichst dich immer
				DIREKT mit dem ANBIETER und bekommst so definitiv die besten DIREKTBUCHERPREISE.
				<br><br><br>

				<p class="mt-3">
				<h2>REISEN ZU DEINEN PREISEN</h2>Hast du dich dann mit deinem Gastgeber geeinigt, erhältst du von uns
				einen hobidd-Code mit welchem du dann den Buchungsvorgang wiederum direkt mit dem jeweiligen Anbieter
				abschliesst.

			</div>
            
		</div>
	</div>
	<div class="container mb-5 mt-5" style="background: #720f7e;">
		<div class="row justify-content-center align-items-center py-4">
			<div class="col px-5">
				<h1 class="big-header">das VIDEO ZUM TEXT</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col mb-5">
				<div align="center">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
								src="https://www.youtube-nocookie.com/embed/4zPoiXrez0Y?rel=0" frameborder="0"
								allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['lang']->value=='en') {?>
    
	<div class="container">
		<div class="row">
			<div class="col-lg-7 pr-lg-8 mb-5 mt-0">
				<br>
				<h2>FIND YOUR FAVORITE STAY</h2>Search for hotels, places, topics or simply use our full text search to
				find desired destinations.
				<br><br><br>

                

				<p class="mt-3">
				<h2>DIRECT BOOKINGS ONLY . WHAT ELSE</h2>Did you find a suitable stay? Perfect, after your free
				registration you can submit up to 5 price suggestions for each of them. You are always in DIRECT contact
				to VENDORS and you will be able catch very best DIRECT BOOKING RATES or receive even better
				counter-offers.
				<br><br><br>

				<p class="mt-3">
				<h2>YOUR PRICE . YOUR VACATIONS</h2>If both agree, you as traveller will get a special hobidd code which
				can be redeemed during your direct booking process. Can’t be easier.
				</p>
			</div>
            
		</div>
	</div>
	<div class="container mb-5 mt-5" style="background: #720f7e;">
		<div class="row justify-content-center align-items-center py-4">
			<div class="col px-5">
				<h1 class="big-header">ACCOMPANYING VIDEO</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col mb-5">
				<div align="center">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
								src="https://www.youtube-nocookie.com/embed/UfYn69cXI1k?rel=0" frameborder="0"
								allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }?>

<?php }} ?>
