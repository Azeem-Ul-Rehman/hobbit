{if $lang == 'de'}
	<br>
	<div class="container">
		<div class="row">
			<div class="col-lg-7 pr-lg-8 mb-5 mt-0">

				<h2>ENTDECKE NEUE REISEZIELE</h2>Suche nach Hotels, Orten oder Ländern oder nutze einfach die
				Volltextsuche um gezielt nach Lastminuteangeboten oder Themenbereichen zu suchen. Einfacher geht es
				nicht.
				<br><br><br>

                {*<p class="mt-3">
                <span class="big-text" style="color:#00a6f7;">2 </span><span class="small-text">You as traveller join draws with your simple Facebook Like. No registration needed. Can’t be easier.</span>
                </p>*}

				<p class="mt-3">{*<h2 style="color:#685c56";> dunkelbraun*}
				<h2>HANDLE DEINEN PREIS AUS</h2>Ist das passende Package gefunden, hast du nach kostenloser
				Registrierung die Möglichkeit jeweils bis zu 5 Preisvorschläge dafür abzugeben. Du besprichst dich immer
				DIREKT mit dem ANBIETER und bekommst so definitiv die besten DIREKTBUCHERPREISE.
				<br><br><br>

				<p class="mt-3">
				<h2>REISEN ZU DEINEN PREISEN</h2>Hast du dich dann mit deinem Gastgeber geeinigt, erhältst du von uns
				einen hobidd-Code mit welchem du dann den Buchungsvorgang wiederum direkt mit dem jeweiligen Anbieter
				abschliesst.

			</div>
            {*<div class="col-lg-5 mb-5 mt-0">
                <img class="img-fluid mx-auto d-block" src="/img/CONTENT/gelb.png">
            </div>*}
		</div>
	</div>
	<div class="container mb-5 mt-5" style="background: #720f7e;">
		<div class="row justify-content-center align-items-center py-4">
			<div class="col px-5">
				<h1 class="big-header">das VIDEO ZUM TEXT</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col mb-5">
				<div align="center">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
								src="https://www.youtube-nocookie.com/embed/4zPoiXrez0Y?rel=0" frameborder="0"
								allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
{/if}

{if $lang == 'en'}
    {*<div class="container mb-5 mt-0" style="background: #dbc4b7;">
        <div class="row justify-content-center align-items-center py-4">
            <div class="col px-5">
                <h1 class="big-header">TRAVELLING AT YOUR PRICES</h1>
            </div>
        </div>
    </div>*}
	<div class="container">
		<div class="row">
			<div class="col-lg-7 pr-lg-8 mb-5 mt-0">
				<br>
				<h2>FIND YOUR FAVORITE STAY</h2>Search for hotels, places, topics or simply use our full text search to
				find desired destinations.
				<br><br><br>

                {*<p class="mt-3">
                <span class="big-text" style="color:#00a6f7;">2 </span>You as traveller join draws with your simple Facebook Like. No registration needed. Can’t be easier.
                </p>*}

				<p class="mt-3">
				<h2>DIRECT BOOKINGS ONLY . WHAT ELSE</h2>Did you find a suitable stay? Perfect, after your free
				registration you can submit up to 5 price suggestions for each of them. You are always in DIRECT contact
				to VENDORS and you will be able catch very best DIRECT BOOKING RATES or receive even better
				counter-offers.
				<br><br><br>

				<p class="mt-3">
				<h2>YOUR PRICE . YOUR VACATIONS</h2>If both agree, you as traveller will get a special hobidd code which
				can be redeemed during your direct booking process. Can’t be easier.
				</p>
			</div>
            {*<div class="col-lg-5 mb-5 mt-0">
                <img class="img-fluid mx-auto d-block" src="/img/CONTENT/gelb.png">
            </div>*}
		</div>
	</div>
	<div class="container mb-5 mt-5" style="background: #720f7e;">
		<div class="row justify-content-center align-items-center py-4">
			<div class="col px-5">
				<h1 class="big-header">ACCOMPANYING VIDEO</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col mb-5">
				<div align="center">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item"
								src="https://www.youtube-nocookie.com/embed/UfYn69cXI1k?rel=0" frameborder="0"
								allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
{/if}

